使い方

1. npm run install
2. node index.js
3. ルートディレクトリに「client_secret.json」を貼り付け
　 ※Google Cloud Platformで作ったやつ
4. コンソールに表示されるURLにアクセスしに認証許可を行う
5. 画面に文字列が表示されるのでコピー
6. コンソールが入力状態で待機しているので4.でコピーした値を入力
7. エンターでファイルが作成される